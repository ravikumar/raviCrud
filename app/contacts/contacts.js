'use strict';

angular.module('myApp.contacts', ['ngRoute', 'firebase'])

.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/contacts', {
		templateUrl: 'contacts/contacts.html',
		controller: 'contactsCtrl'
	});
}])

.controller('contactsCtrl', ['$scope', '$firebaseArray', function($scope, $firebaseArray) {
	//Intialize firebase
	var ref = new Firebase('https://ravicrud.firebaseio.com/contacts');

	//get contacts
	$scope.contacts = $firebaseArray(ref);
	//console.log($scope.contacts);

	//show contact form
	$scope.showContactForm = function () {
		$scope.showForm = true;
	}

	//hide contact form
	$scope.hideContactForm = function(){
		$scope.showForm = false;	
		$scope.showContact = false;
		$scope.updateForm = false;
	}

	//Show update form
	$scope.showEditContactForm = function(contact){
		$scope.updateForm = true;
		$scope.showForm = false;
		$scope.id = contact.$id;
		$scope.name = contact.name;
		$scope.company = contact.company;
		$scope.email = contact.email;
	}



	//submit contact form
	$scope.submitContactForm = function(){
		//console.log('Adding....');

		//Assign values
		if ($scope.name) {
			var name = $scope.name;
		}else{
			var name = null;
		}
		if ($scope.company) {
			var company = $scope.company;
		}else{
			var company = null;
		}
		if ($scope.email) {
			var email = $scope.email;
		}else{
			var email = null;
		}


		//build objects
		$scope.contacts.$add({
			name : name,
			company : company,
			email : email
		}).then(function(ref){
			var id = ref.key();
			console.log('Form added with id:' + id);

			//call clearContactFormField method
			clearContactFormField();

			//hide contact form
			$scope.showForm = false;

			//Show Message
			$scope.message = "Form added..."
		});
	};


	//update form
	$scope.editContactFormSubmit = function(){
		var id = $scope.id;
		var record = $scope.contacts.$getRecord(id);

		record.name = $scope.name;
		record.company = $scope.company;
		record.email = $scope.email;
		$scope.contacts.$save(record).then(function(ref){
			console.log(ref.key);
		})

		clearContactFormField();

		$scope.updateForm = false;

		$scope.message = "Updated..."

	}


	//Remove Contact
	$scope.removeContactForm  = function(contact){
		$scope.contacts.$remove(contact);
		$scope.message = "Contact Removed";
	}


	//show details
	$scope.showContactDetails = function(contact){
		$scope.name = contact.name;
		$scope.company = contact.company;
		$scope.email = contact.email;

		$scope.showContact = true;
	}

	//clear form field
	function clearContactFormField(){
		console.log('clearing all field');

		$scope.name = '';
		$scope.company = '';
		$scope.email = '';
	}

}]);